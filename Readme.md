# service-node-runner

## Purpose

This service can be used to run nodejs _very_ crudely in docker environments, such as rancher.
Simply deploy a service where the whole script is exposed as an environment variable, done.

**Note**: This is supposed to make live easy while developing, **DO NOT USE THIS IN PRODUCTION**.
