FROM mhart/alpine-node:11

RUN npm install -g yarn && \
    mkdir /app

WORKDIR /app
ADD entrypoint.sh /app
ENTRYPOINT /app/entrypoint.sh