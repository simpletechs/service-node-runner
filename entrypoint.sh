#!/bin/sh
PACKAGE_JSON=${PACKAGE_JSON:-"{}"}
INDEX_JS=${INDEX_JS:-"console.error('You need to specify the INDEX_JS environment variable!'); process.exit(1);"}
echo $PACKAGE_JSON > package.json
echo $INDEX_JS > index.js

yarn && \
yarn add esm && \
node -r esm .